﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestConsole2.ball;

namespace TestConsole2
{
    class Program
    {
        static void Main(string[] args)
        {
            var ball = new Ball();
            ball.Start();
            System.Diagnostics.Stopwatch sw = new System.Diagnostics.Stopwatch();
            sw.Start();
            long lastFrame = 0;

            while (true)
            {
                ball.Update();
                Time.elepsed = sw.ElapsedMilliseconds - lastFrame;
                lastFrame = sw.ElapsedMilliseconds;
                if (lastFrame < 34) System.Threading.Thread.Sleep(100 - (int)lastFrame);
            }

        }

    }
}
