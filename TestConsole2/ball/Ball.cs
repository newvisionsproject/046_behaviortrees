﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NvpFluentBehaviorTree;

namespace TestConsole2.ball
{
    public class Ball
    {
        private Transform _transform;
        private NvpFluentBehaviorTree.IBehaviorTreeNode _tree;
        private Vector3 _direction = new Vector3(1f, 1f, 0f);

        public void Start()
        {
            _transform = new Transform();
            _transform.Position = new Vector3(0f, 0f, 0f);
            var builder = new BehaviourTreeBuilder();
            _tree = builder
                .Parallel("ball-move", 10, 10)
                    .Sequence("sense-wall")
                        .Do("check-bounds", t =>
                        {
                            if (Math.Abs(_transform.Position.y) > 10)
                                return BehaviorTreeStatus.Success;
                            else return BehaviorTreeStatus.Failure;
                        })
                        .Do("invert-vertical-direction", t =>
                        {
                            _direction.y *= -1;
                            return BehaviorTreeStatus.Success;
                        })
                        .Do("reset-vertical-position", t =>
                        {
                            if (_transform.Position.y > 10) _transform.Position.y = 10;
                            if (_transform.Position.y < -10) _transform.Position.y = -10;
                            return BehaviorTreeStatus.Success;
                        })
                        .End()
                    .Sequence("sense-bounds")
                        .Do("sense-left-right", t =>
                        {
                            if (Math.Abs(_transform.Position.x) > 20)
                                return BehaviorTreeStatus.Success;
                            else
                                return BehaviorTreeStatus.Failure;
                        })
                        .Do("invert-horizontal-direction", t =>
                        {
                            _direction.x *= -1f;
                            return BehaviorTreeStatus.Success;
                        })
                        .Do("reset-horizontal-direction", t =>
                        {
                            if (_transform.Position.x > 20f)
                                _transform.Position.x = 20f;
                            else
                                _transform.Position.x = -20f;
                            return BehaviorTreeStatus.Success;
                        })
                        .End()
                    .Do("move", t =>
                    {
                        Vector3 temp = new Vector3(
                        _direction.x * Time.deltaTime,
                        -_direction.y * Time.deltaTime,
                        0);
                        _transform.Translate(temp);
                        return BehaviorTreeStatus.Success;
                    })
                .End()
                .Build();
        }

        public void Update()
        {
            TimeData temp = new TimeData();
            temp.deltaTime = Time.deltaTime;
            _tree.Tick(temp);
            //Console.Clear();
            Console.WriteLine($"x: {_transform.Position.x}\ny :{_transform.Position.y}\n");
        }


    }

    public static class Time
    {
        
        public static long elepsed;

        public static float deltaTime => elepsed/1000f;
    }

    public class Vector3
    {
        public float x;
        public float y;
        public float z;


        public Vector3(float x, float y, float z)
        {
            this.x = x;
            this.y = y;
            this.z = z;
        }
    }

    public class Transform
    {
        public Vector3 Position;

        public void Translate(Vector3 translationVector)
        {
            this.Position.x += translationVector.x;
            this.Position.y += translationVector.y;
            this.Position.z += translationVector.z;
        }
    }
}
