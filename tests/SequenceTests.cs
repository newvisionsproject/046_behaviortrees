﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Moq;
using NUnit.Framework;
using NvpFluentBehaviorTree;
using NvpFluentBehaviorTree.Nodes;

namespace tests
{
    [TestFixture]
    public class SequenceTests
    {

        SequenceNode testObject;

        public void Init()
        {
            testObject = new SequenceNode("test-sequence");
        }

        [Test]
        public void SequenceNodeTests_run_all_children_in_order()
        {
            Init();

            var time = new TimeData();

            int callOrder = 0;

            var mNode1 = new Mock<IBehaviorTreeNode>();
            mNode1
                .Setup(m => m.Tick(time))
                .Returns(BehaviorTreeStatus.Success)
                .Callback(() =>
                {
                    Assert.AreEqual(1, ++callOrder);
                });

            var mNode2 = new Mock<IBehaviorTreeNode>();
            mNode2
                .Setup(m => m.Tick(time))
                .Returns(BehaviorTreeStatus.Success)
                .Callback(() =>
                {
                    Assert.AreEqual(2, ++ callOrder);
                });

            testObject.AddChildNode(mNode1.Object);
            testObject.AddChildNode(mNode2.Object);

            testObject.Tick(time);

            Assert.AreEqual(2, callOrder);

            mNode1.Verify(m => m.Tick(time), Times.Once());
            mNode2.Verify(m => m.Tick(time), Times.Once());

        }

        [Test]
        public void SequenceNodeTests_when_first_child_is_running_2_child_is_suppressed()
        {
            Init();
            var time = new TimeData();

            var node1 = new Mock<IBehaviorTreeNode>();
            node1.Setup(m => m.Tick(time))
                .Returns(BehaviorTreeStatus.Running);

            var node2 = new Mock<IBehaviorTreeNode>();
            node2.Setup(m => m.Tick(time))
                .Returns(BehaviorTreeStatus.Failure);

            testObject.AddChildNode(node1.Object);
            testObject.AddChildNode(node2.Object);

            BehaviorTreeStatus result = testObject.Tick(time);
            Assert.AreEqual(BehaviorTreeStatus.Running, result);

            node1.Verify(m => m.Tick(time), Times.Once());
            node2.Verify(m => m.Tick(time), Times.Never());

        }

    }
}
