﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Moq;
using NUnit.Framework;
using NvpFluentBehaviorTree;
using NvpFluentBehaviorTree.Nodes;

namespace tests
{
    [TestFixture]
    public class ParallelNodeTests
    {
        public ParallelNode testNode;

        public void Init(int numRequiredToFail = 0, int numRequiredToSucceed = 0)
        {
            testNode = new ParallelNode("test-parallel", numRequiredToFail, numRequiredToSucceed);
        }

        [Test]
        public void ParallelNode_runs_all_nodes_in_order()
        {
            Init();

            var time = new TimeData();

            var callOrder = 0;

            var mockChildNode1 = new Mock<IBehaviorTreeNode>();
            mockChildNode1
                .Setup(m => m.Tick(time))
                .Returns(BehaviorTreeStatus.Running)
                .Callback(() =>
                {
                    Assert.AreEqual(1, ++callOrder);
                });

            var mockChildNode2 = new Mock<IBehaviorTreeNode>();
            mockChildNode2
                .Setup(m => m.Tick(time))
                .Returns(BehaviorTreeStatus.Running)
                .Callback(() =>
                {
                    Assert.AreEqual(2, ++callOrder);
                });

            testNode.AddChildNode(mockChildNode1.Object);
            testNode.AddChildNode(mockChildNode2.Object);

            BehaviorTreeStatus state = testNode.Tick(time);
            Assert.AreEqual(BehaviorTreeStatus.Running, state);

            Assert.AreEqual(2, callOrder);

            mockChildNode1.Verify(m => m.Tick(time), Times.Once());
            mockChildNode2.Verify(m => m.Tick(time), Times.Once());


        }

        [Test]
        public void ParallelNode_required_num_of_children_fail()
        {
            Init(2, 2);

            var time = new TimeData();

            var mockNode1 = new Mock<IBehaviorTreeNode>();
            mockNode1
                .Setup(m => m.Tick(time))
                .Returns(BehaviorTreeStatus.Failure);

            var mockNode2 = new Mock<IBehaviorTreeNode>();
            mockNode2
                .Setup(m => m.Tick(time))
                .Returns(BehaviorTreeStatus.Failure);

            var mockNode3 = new Mock<IBehaviorTreeNode>();
            mockNode3
                .Setup(m => m.Tick(time))
                .Returns(BehaviorTreeStatus.Running);

            testNode.AddChildNode(mockNode1.Object);
            testNode.AddChildNode(mockNode2.Object);
            testNode.AddChildNode(mockNode3.Object);

            BehaviorTreeStatus resultState = testNode.Tick(time);
            Assert.AreEqual(BehaviorTreeStatus.Failure, resultState);

            mockNode1.Verify(m => m.Tick(time), Times.Once());
            mockNode2.Verify(m => m.Tick(time), Times.Once());
            mockNode3.Verify(m => m.Tick(time), Times.Once());
        }
    }
}
