﻿using System;
using UnityEngine;

namespace NvpFluentBehaviorTree
{
    public struct TimeData
    {
        public float deltaTime;

        public TimeData(float deltaTime)
        {
            this.deltaTime = deltaTime;
        }
    }
}
