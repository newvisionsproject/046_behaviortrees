﻿namespace NvpFluentBehaviorTree
{
    public interface IBehaviorTreeNode
    {
        BehaviorTreeStatus Tick(TimeData time);
    }
}
