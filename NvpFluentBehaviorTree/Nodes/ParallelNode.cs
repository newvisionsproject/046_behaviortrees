using System.Collections.Generic;

namespace NvpFluentBehaviorTree.Nodes
{
    public class ParallelNode : IParentBehaviorTreeNode
    {
        private string _name;

        private List<IBehaviorTreeNode> _childNodes = new List<IBehaviorTreeNode>();
        private int _numRequiredToFail;
        private int _numRequiredToSucceed;


        public ParallelNode(string name, int numRequiredToFail, int numRequiredToSucceed)
        {
            _name = name;
            _numRequiredToFail = numRequiredToFail;
            _numRequiredToSucceed = numRequiredToSucceed;
        }

        public BehaviorTreeStatus Tick(TimeData deltaTime)
        {
            var numChildrenSucceeded = 0;
            var numChildrenFailed = 0;

            foreach (var childNode in _childNodes)
            {
                var childState = childNode.Tick(deltaTime);
                switch (childState)
                {
                    case BehaviorTreeStatus.Success:
                        numChildrenSucceeded++;
                        break;
                    case BehaviorTreeStatus.Failure:
                        numChildrenFailed++;
                        break;
                }
            }

            if (numChildrenSucceeded > 0 && numChildrenSucceeded >= _numRequiredToSucceed)
                return BehaviorTreeStatus.Success;

            if (numChildrenFailed > 0 && numChildrenFailed >= _numRequiredToFail)
                return BehaviorTreeStatus.Failure;

            return BehaviorTreeStatus.Running;
        }

        public void AddChildNode(IBehaviorTreeNode childNode)
        {
            _childNodes.Add(childNode);
        }
    }
}