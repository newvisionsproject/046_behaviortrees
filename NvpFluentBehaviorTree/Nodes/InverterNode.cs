namespace NvpFluentBehaviorTree.Nodes
{
    public class InverterNode : IParentBehaviorTreeNode
    {
        private string _name;
        private IBehaviorTreeNode _childNode;

        public InverterNode(string name)
        {
            _name = name;
        }

        public BehaviorTreeStatus Tick(TimeData deltaTime)
        {
            if (_childNode == null) throw new System.ApplicationException("InverterNode: must have a child node!");

            BehaviorTreeStatus childStatus = _childNode.Tick(deltaTime);

            // if other than running, return inverted status
            return childStatus == BehaviorTreeStatus.Running
                ? BehaviorTreeStatus.Running
                : childStatus == BehaviorTreeStatus.Success
                    ? BehaviorTreeStatus.Failure
                    : BehaviorTreeStatus.Success;

        }

        public void AddChildNode(IBehaviorTreeNode childNode)
        {
            if (_childNode != null)
                throw new System.ApplicationException("InverterNode: Adding multiple child nodes is prohibited");

            _childNode = childNode;
        }
    }
}