using System.Collections.Generic;

namespace NvpFluentBehaviorTree.Nodes
{
    public class SequenceNode : IParentBehaviorTreeNode
    {
        private string _name;

        private List<IBehaviorTreeNode> _childNodes = new List<IBehaviorTreeNode>();

        public SequenceNode(string name)
        {
            _name = name;
        }

        public BehaviorTreeStatus Tick(TimeData deltaTime)
        {
            foreach (IBehaviorTreeNode childNode in _childNodes)
            {
                BehaviorTreeStatus childStatus = childNode.Tick(deltaTime);
                if (childStatus != BehaviorTreeStatus.Success) return childStatus;
            }

            return BehaviorTreeStatus.Success;
        }

        public void AddChildNode(IBehaviorTreeNode childNode)
        {
            _childNodes.Add(childNode);
        }
    }
}