using System.Collections.Generic;

namespace NvpFluentBehaviorTree.Nodes
{
    public class SelectorNode : IParentBehaviorTreeNode
    {
        private string _name;
        private readonly List<IBehaviorTreeNode> _childNodes = new List<IBehaviorTreeNode>();

        public SelectorNode(string name)
        {
            _name = name;
        }

        public BehaviorTreeStatus Tick(TimeData deltaTime)
        {
            foreach (IBehaviorTreeNode childNode in _childNodes)
            {
                var childStatus = childNode.Tick(deltaTime);
                if (childStatus == BehaviorTreeStatus.Failure) return childStatus; 
            }

            return BehaviorTreeStatus.Failure;
        }

        public void AddChildNode(IBehaviorTreeNode childNode)
        {
            _childNodes.Add(childNode);
        }
    }
}