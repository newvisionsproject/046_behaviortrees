﻿using System;
using System.Linq;
using System.Text;

namespace NvpFluentBehaviorTree.Nodes
{
    public class ActionNode : IBehaviorTreeNode
    {
        private string name;

        private Func<TimeData, BehaviorTreeStatus> fn;

        public ActionNode(string name, Func<TimeData, BehaviorTreeStatus> fn)
        {
            this.name = name;
            this.fn = fn;
        }

        public BehaviorTreeStatus Tick(TimeData time)
        {
            return fn(time);
        }
    }
}
