﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NvpFluentBehaviorTree
{
    public interface IParentBehaviorTreeNode : IBehaviorTreeNode
    {
        void AddChildNode(IBehaviorTreeNode childNode);
    }
}
