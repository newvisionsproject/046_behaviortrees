﻿namespace NvpFluentBehaviorTree
{
    public enum BehaviorTreeStatus
    {
        Success,
        Failure,
        Running
    }
}