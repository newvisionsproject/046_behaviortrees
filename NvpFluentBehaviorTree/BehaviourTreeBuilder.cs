﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NvpFluentBehaviorTree.Nodes;

namespace NvpFluentBehaviorTree
{
    public class BehaviourTreeBuilder
    {
        private IBehaviorTreeNode lastNode = null;
        private Stack<IParentBehaviorTreeNode> parentNodeStack = new Stack<IParentBehaviorTreeNode>();

        public BehaviourTreeBuilder Do(string name, Func<TimeData, BehaviorTreeStatus> fn)
        {
            if (parentNodeStack.Count <= 0)
            {
                throw new ApplicationException("Can't create unnested ActionNode, it must be a leaf node.");
            }

            var actionNode = new ActionNode(name, fn);
            parentNodeStack.Peek().AddChildNode(actionNode);
            return this;
        }

        public BehaviourTreeBuilder Condition(string name, Func<TimeData, bool> fn)
        {
            return Do(name, t => fn(t) ? BehaviorTreeStatus.Success : BehaviorTreeStatus.Failure);
        }

        public BehaviourTreeBuilder Inverter(string name)
        {
            var inverterNode = new InverterNode(name);

            if (parentNodeStack.Count > 0) parentNodeStack.Peek().AddChildNode(inverterNode);

            parentNodeStack.Push(inverterNode);
            return this;
        }

        public BehaviourTreeBuilder Sequence(string name)
        {
            var sequenceNode = new SequenceNode(name);

            if (parentNodeStack.Count > 0)
            {
                parentNodeStack.Peek().AddChildNode(sequenceNode);
            }

            parentNodeStack.Push(sequenceNode);
            return this;
        }

        public BehaviourTreeBuilder Parallel(string name, int numRequiredToFail, int numRequiredToSucceed)
        {
            var parallelNode = new ParallelNode(name, numRequiredToFail, numRequiredToSucceed);

            if (parentNodeStack.Count > 0)
            {
                parentNodeStack.Peek().AddChildNode(parallelNode);
            }

            parentNodeStack.Push(parallelNode);
            return this;
        }

        public BehaviourTreeBuilder Selector(string name)
        {
            var selectorNode = new SelectorNode(name);

            if (parentNodeStack.Count > 0)
            {
                parentNodeStack.Peek().AddChildNode(selectorNode);
            }
            parentNodeStack.Push(selectorNode);
            return this;
        }

        public BehaviourTreeBuilder Splice(IBehaviorTreeNode subTree)
        {
            if (subTree == null)
            {
                throw new ApplicationException("subtree");
            }

            if (parentNodeStack.Count <= 0)
            {
                throw new ApplicationException("Can't splice an unnested sub-tree, there must be a parent-tree");
            }

            parentNodeStack.Peek().AddChildNode(subTree);
            return this;
        }

        public IBehaviorTreeNode Build()
        {
            if (lastNode == null)
            {
                throw new ApplicationException("Can't create behavior tree with zero nodes");
            }
            return lastNode;
        }

        public BehaviourTreeBuilder End()
        {
            lastNode = parentNodeStack.Pop();
            return this;
        }
    }
}
